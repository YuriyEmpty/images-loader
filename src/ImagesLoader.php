<?php
/**
 * Create by: Yuriy Empty
 * Date: 06.04.2019
 * Time: 23:56
 */

namespace Yuriyempty\ImagesLoader;


class ImagesLoader
{

    /**
     * Formats array
     *
     * @var array
     */
    private $formats = ["png", "jpg", "gif"];

    /**
     * Maximum file size allowed, used if $checkSize = true;
     *
     * @var int 2mb default
     */
    private $sizeLimit = 2000000;

    /**
     * If this option is true, file size will be checked < $sizeLimit before download
     *
     * @var bool
     */
    private $checkSize = false;

    /**
     * @param string $url
     * @param string $directory
     * @param string $name
     * @return string
     * @throws \Exception
     */
    public function loadImage(string $url, string $directory, $name = "")
    {
        $this->validateUrl($url);
        $this->validateDirectory($directory);

        $fileInfo = @get_headers($url, true);

        $this->validateFile($fileInfo, $directory);

        if(!$name) {
            $name = $this->determineName($url, $fileInfo);
        } else {
            $name .= ".".$this->getFormat($fileInfo);
        }

        $this->write($url, $directory.$name);

        return $directory.$name;
    }

    /**
     * Use for validate file before download to server
     *
     * @param $fileInfo
     * @param $directory
     * @throws \Exception
     */
    protected function validateFile($fileInfo, $directory)
    {

        if(!strpos($fileInfo[0], "200")) {
            throw new \InvalidArgumentException("File not found");
        }

        if($this->checkSize) {
            $this->validateSize($fileInfo);
        }

        if(!in_array($this->getFormat($fileInfo), $this->formats)) {
            throw new \InvalidArgumentException("Incorrect file format");
        }

    }

    /**
     * @param $url
     */
    protected function validateUrl(&$url)
    {
        $url = preg_replace("/\?.+/", "", $url);
    }

    protected function validateDirectory(&$directory)
    {
        if(substr($directory,-1) != "/") {
            $directory .= "/";
        }

        if(!file_exists($directory)) {
            throw new \InvalidArgumentException("The save directory does not exist");
        }
    }

    /**
     * @param $fileInfo
     * @return string
     */
    protected function getFormat($fileInfo)
    {
        $format = [];
        preg_match_all("/\w+\w+/", $fileInfo["Content-Type"], $format);
        $format = $format[0][1];

        if($format == "plain") {
            $format = "gif";
        } elseif ($format == "jpeg") {
            $format = "jpg";
        }

        return $format;
    }

    protected function validateSize($fileInfo)
    {
        if(!$fileInfo["Content-Length"]) {
            throw new \InvalidArgumentException("Could not determine file size");
        }

        if($fileInfo["Content-Length"] > $this->sizeLimit) {
            throw new \LengthException("File size is larger than allowed");
        }

    }

    /**
     * @param $url
     * @param $fileInfo
     * @return string
     */
    protected function determineName($url, $fileInfo)
    {
        $name = "";
        $format = $this->getFormat($fileInfo);
        if(isset($fileInfo["Content-Disposition"])) {
            preg_match_all("/\w+\.\w+/", $fileInfo["Content-Disposition"], $name);
            return $name[0][0];
        }

        $name = basename($url);

        $existFormat = [];
        preg_match_all("/.[^\.\/]+$/",$name ,$existFormat );

        if($existFormat[0][0] != ".".$format) {
            $name .= ".".$format;
        }

        return $name;
    }

    /**
     * @param $url
     * @param $directory
     */
    protected function write($url, $directory)
    {
        file_put_contents($directory, file_get_contents($url));
    }

    /**
     *Expect fractional value in megabytes
     *
     * @param int $size
     */
    public function setSizeLimit(float $size)
    {
        $this->sizeLimit = $size * 1000000;
    }

    /**
     * @return int bytes
     */
    public function getSizeLimit()
    {
        return $this->sizeLimit;
    }

    /**
     * @param bool $check
     */
    public function setCheckSize(bool $check)
    {
        $this->checkSize = $check;
    }

    /**
     * @return bool
     */
    public function getCheckSize()
    {
        return $this->checkSize;
    }

}