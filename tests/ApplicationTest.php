<?php
/**
 * Create by: Yuriy Empty
 * Date: 07.04.2019
 * Time: 20:09
 */


use \PHPUnit\Framework\TestCase;
use \Yuriyempty\ImagesLoader\ImagesLoader;

class ApplicationTest extends TestCase
{

    const DOCUMENT_ROOT = "E:\OSPanel\domains\images-loader";

    /**
     * @throws Exception
     */
    public function testDownload()
    {
        $imagesLoader = new ImagesLoader();

        $resultJpg = $imagesLoader->loadImage("https://cdn.friendsoftheearth.uk/sites/default/files/styles/hero_image/public/media/images/sylwia-bartyzel-135274.jpg?itok=IP4UXGU2" ,self::DOCUMENT_ROOT."/tests/images");
        $resultPng = $imagesLoader->loadImage("http://clipart-library.com/image_gallery2/Nature-Free-PNG-Image.png" ,self::DOCUMENT_ROOT."/tests/images");
        $resultGif = $imagesLoader->loadImage("https://media.giphy.com/media/8h943ofFHgJPy/giphy.gif" ,self::DOCUMENT_ROOT."/tests/images");

        $this->assertFileExists($resultJpg);
        $this->assertFileExists($resultPng);
        $this->assertFileExists($resultGif);
    }

    /**
     * @expectedException InvalidArgumentException
     *
     * Directory don't exist
     */
    public function testDirectoryNotExists()
    {
        $imagesLoader = new ImagesLoader();

        $imagesLoader->loadImage("https://media.giphy.com/media/8h943ofFHgJPy/giphy.gif" ,self::DOCUMENT_ROOT."/tests/img/");
    }

    /**
     * @expectedException InvalidArgumentException
     *
     * If remote file don't exists
     */
    public function testFileNotExists()
    {
        $imagesLoader = new ImagesLoader();

        $imagesLoader->loadImage("https://www.google.com/images/test.jpg" ,self::DOCUMENT_ROOT."/tests/images");

    }

    /**
     * @expectedException LengthException
     *
     * If remote file size > default
     */
    public function testIllegalFileSize()
    {
        $imagesLoader = new ImagesLoader();
        $imagesLoader->setCheckSize(true);

        $imagesLoader->loadImage("https://cdn.wallscloud.net/converted/3917605666-Les-V-4k-jOyR-3440x1440-MM-100.jpg" ,self::DOCUMENT_ROOT."/tests/images"); //3.6 mb

    }

    /**
     *
     * If remote file don't exists
     */
    public function testChangeDefaultFileSize()
    {
        $imagesLoader = new ImagesLoader();
        $imagesLoader->setCheckSize(true);
        $imagesLoader->setSizeLimit(3.7); //mb
        echo $imagesLoader->getSizeLimit();

        $result = $imagesLoader->loadImage("https://cdn.wallscloud.net/converted/3917605666-Les-V-4k-jOyR-3440x1440-MM-100.jpg" ,self::DOCUMENT_ROOT."/tests/images"); //3.6 mb

        $this->assertFileExists($result);

    }

    /**
     *@expectedException InvalidArgumentException
     *
     * Illegal file format, try to download .zip book https://www.litmir.me
     */
    public function testFileFormat()
    {
        $imagesLoader = new ImagesLoader();

        $imagesLoader->loadImage("https://www.litmir.me/data/Book/0/642000/642858/Kazakov_JUrii_Mankabr_%28Rasskazy%29_Litmir.net_642858_4a7f5.pdf.zip" ,self::DOCUMENT_ROOT."/tests/images");
    }

    /**
     * For clear downloaded images
     */
    public function testClear() {
        $files = glob(self::DOCUMENT_ROOT."/tests/images/*");
        foreach($files as $file){
            if(is_file($file)) {
                unlink($file);
            }

        }
    }
}